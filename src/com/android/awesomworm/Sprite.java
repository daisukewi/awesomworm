package com.android.awesomworm;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;

/**
 * @author Daniel
 *
 */
public class Sprite {
	private static int MARGEN = 15;
	
	public int destX;
	public int destY;
	public int origX;
	public int origY;
	public int posX;
	public int posY;
	public int width;
	public int height;
	public float rot;
	private int anchoPantalla;
	private int altoPantalla;
	public Bitmap imagen;

	
	/**
	 * Crea un Sprite con una imagen dada.
	 */
	public Sprite(int posX, int posY, int destX, int destY, Bitmap imagen) {
		this.posX = posX;
		this.posY = posY;
		this.destX = destX;
		this.destY = destY;
		this.origX = posX;
		this.origY = posY;
		
		this.width = imagen.getWidth();
		this.height = imagen.getHeight();
		this.rot = 0;
		this.imagen = getImagenRotada(this.imagen);
	}
	
	/**
	 * Crea un Sprite en las coordenadas dadas.
	 */
	public Sprite (int posX, int posY, Bitmap imagen) {
		this.posX = posX;
		this.posY = posY;
		this.origX = posX;
		this.origY = posY;
		this.imagen = imagen;
		this.width = imagen.getWidth();
		this.height = imagen.getHeight();
	}
	
	public void setDestino(int destX, int destY) {
		this.destX = destX;
		this.destY = destY;
		
		this.rot = 0;
		this.imagen = getImagenRotada(this.imagen);
	}
	
	public Bitmap getImagenRotada(Bitmap imgOrig) {
		Matrix matriz = new Matrix();
		
		matriz.postRotate(this.rot);
		return Bitmap.createBitmap(imgOrig, 0, 0, width, height, matriz, true);
	}
	
	public void setDimensionesPantalla (int anchoPantalla, int altoPantalla) {
		this.anchoPantalla = anchoPantalla;
		this.altoPantalla = altoPantalla;
	}
	
	/**
	 * Hace avanzar al sprite una velocidad dada y si ha llegado a su destino
	 * devuelve true.
	 * @param velocidad posiciones que avanza en diagonal el sprite
	 * @return true si ha llegado a su destino
	 */
	public boolean avanzar (int velocidad) {
		float n_turnos;
		float dist_total;
		float vel_x, vel_y;
		float distX = destX - origX;
		float distY = destY - origY;
		
		// Calculamos el numero de turnos que tarda el sprite en llegar a su destino
		dist_total = (float)Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2));
		n_turnos = dist_total / velocidad;
		
		// Con el numero de turnos y la distancia total, calculamos la distancia recorrida por turno
		vel_x = distX / n_turnos;
		vel_y = distY / n_turnos;
		
		// Sumamos la distancia recorrida a su posici�n
		posX += vel_x;
		posY += vel_y;
		
		// Comprobamos si se ha salido de la pantalla
		if (posX < (0 - width - 5) || posX > (anchoPantalla + width + 5) || posY < (0 - height - 5) || posY > (anchoPantalla + height + 5))
			return true;

		// En caso de que no haya llegado a su destino, devolvemos false
		return false;
	}
	
	/**
	 * Comprueba si este sprite se intersecta horizontalmente con otro sprite
	 */
	private boolean hayInterseccionHorizontal (Sprite s) {
		Sprite izq, der;
		
		if (this.posX < s.posX) {
			izq = this;
			der = s;
		} else {
			izq = s;
			der = this;
		}

		return der.posX < izq.posX + izq.width - MARGEN;
	}
	
	/**
	 * Comprueba si este sprite se intersecta verticalmente con otro sprite
	 */
	private boolean hayInterseccionVertical (Sprite s) {
		Sprite alto, bajo;

		if (this.posY < s.posY) {
			alto = this;
			bajo = s;
		} else {
			alto = s;
			bajo = this;
		}

		return bajo.posY < alto.posY + alto.height - MARGEN;
	}
	
	/**
	 * Comprueba si el Sprite colisiona con otro Sprite dado
	 * @param s el sprite con el que se quiere comprobar la colision
	 * @return true si colisionan
	 */
	public boolean collide (Sprite s) {		
		if (hayInterseccionHorizontal(s) && hayInterseccionVertical(s))
			return true;
		
		return false;
	}

	/**
	 * Acerca el propio sprite a otro moviendolo a una distancia de
	 * la mitad de la que los separa
	 */
	public void moverRespecto(Sprite cabeza, int ralentizacion) {
		if (this.posX + this.width / 2 < cabeza.posX + cabeza.width / 2 || this.posX + this.width / 2 > cabeza.posX + cabeza.width / 2)
			this.posX += (cabeza.posX + cabeza.width / 2 - this.posX - this.width / 2) / ralentizacion;
		if (this.posY + this.height / 2 < cabeza.posY + cabeza.height / 2 || this.posY + this.height / 2 > cabeza.posY + cabeza.height / 2)
			this.posY += (cabeza.posY + cabeza.height / 2 - this.posY - this.height / 2) / ralentizacion;
	}

}
