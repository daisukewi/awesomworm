package com.android.awesomworm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class AwesomWormGame extends View {

	/*** Constantes ***/
	// velocidad de cada mariquta-mala
    private static final int PASO_MOVIMIENTO = 3;
    private static final int MAX_MARIQUITAS = 10;
    private static final int MAX_GOTAS = 3;
    private static final long UPDATE_DELAY = 30;
    private static final int N_COLAGUSANO = 5;
    private static final int MOVIMIENTO_COLA = 10;
    
    /*** Variables del Programa ***/
    private Random rand;
    // Dimensiones de la pantalla del dispositivo
    public int anchoPantalla, altoPantalla;
    // Imagenes que dibujaremos en la pantalla
    private Bitmap img_worm;
    private Bitmap img_mariquita;
    private Bitmap img_gota;
    private Bitmap img_colagusano;
    // Fuente del texto que escribiremos en pantalla
    private Paint mTextPaint;
    private Paint mObjetoPaint;
    // Estado de pausa del juego
    private boolean en_pausa;
    // Probabilidad m�nima de que salga una mariquita-mala
    private int prob_min;
    // Probabilidad de que salga una mariquita-mala
    private int probabilidad;
    // Puntuaci�n del jugador
    private long score;
    
    // Variables para controlar el tiempo
    private double last_time;
    private long elapsed_frames;
    private double init_time;
    private float fps;
    
    private Timer update_timer;
    
    // Objetos del juego
    private ArrayList<Sprite> mariquitas_malas;
    private ArrayList<Sprite> gotas;
    private Sprite gusano;
    private Sprite[] colagusano;
    
    private int n_mariquitas;
    private int n_gotas;
    
	/**
	 * @param context
	 * @param attrs
	 */
	public AwesomWormGame(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		img_worm = BitmapFactory.decodeResource(getResources(), R.drawable.awsm_worm);
		img_mariquita = BitmapFactory.decodeResource(getResources(), R.drawable.mariquita);
		img_colagusano = BitmapFactory.decodeResource(getResources(), R.drawable.cola_gusano);
		img_gota = BitmapFactory.decodeResource(getResources(), R.drawable.pompa);
		setFocusable(true);   // Para asegurarnos que recibimos las pulsaciones de las teclas
		setFocusableInTouchMode(true);
		
		rand = new Random(System.currentTimeMillis());
		initGame();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		// Guardar las nuevas dimensiones de pantalla
		//y resituar nuestro objeto.
		anchoPantalla = w;
        altoPantalla = h;
        gusano.posX = (anchoPantalla / 2) - (gusano.width / 2);
        gusano.posY = (altoPantalla / 2) - (gusano.height / 2);
        
        Iterator<Sprite> is = mariquitas_malas.iterator();
        while (is.hasNext()) {
        	Sprite mariquita = is.next();
        	mariquita.setDimensionesPantalla(anchoPantalla, altoPantalla);
        }
	}
	
	private class UpdateTask extends TimerTask {
		
		@Override
		public void run() {
			double time_since_last_draw = System.currentTimeMillis() - last_time;
			
			fps = (float)(1000 / time_since_last_draw);
			fps = Math.round(fps * 100) / 100.0f;
			last_time = System.currentTimeMillis();
			if (!en_pausa)
				doUpdate();

			postInvalidate();
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		//double time_since_last_draw = System.currentTimeMillis() - last_time;
		
		if (!en_pausa) {
			// Hacemos 60 fps para no cargar al procesador y porque no necesitamos mas
			/*if (time_since_last_draw >= 16.67) {
				fps = (float)(1000 / time_since_last_draw);
				fps = Math.round(fps * 100) / 100.0f;
				last_time = System.currentTimeMillis();
				doUpdate();
				doDraw(canvas);
			}*/
			doDraw(canvas);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		boolean procesada = false;
		float pressX = event.getX();
		float pressY = event.getY();

		if (pressX <= gusano.posX + gusano.width
				&& pressX >= gusano.posX
				&& pressY <= gusano.posY + gusano.height
				&& pressY >= gusano.posY) {
			gusano.posX = (int)(pressX - (gusano.width / 2));
			gusano.posY = (int)(pressY - (gusano.height / 2));
			procesada = true;
			
			// Si nos salimos de la pantalla, corregimos la posici�n
	        // para que no aparezca fuera.
	        if (gusano.posY < 0)
	            gusano.posY = 0;
	        if (gusano.posX < 0)
	            gusano.posX = 0;
	        if (gusano.posY + gusano.height > altoPantalla)
	            gusano.posY = altoPantalla - gusano.height;
	        if (gusano.posX + gusano.width > anchoPantalla)
	            gusano.posX = anchoPantalla - gusano.width;
		}
		
		return procesada;
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		en_pausa = !hasWindowFocus;
	}
	
	
	/****************************************
	 * 				JUEGO					*
	 ****************************************/
	
	private void initGame() {
		// Cargamos el estilo del texto para escribir y dibujar
		mTextPaint = new Paint();
		mTextPaint.setARGB(255, 255, 255, 255);
		mTextPaint.setTextSize(20f);
		mObjetoPaint = new Paint();
		mObjetoPaint.setARGB(255, 255, 255, 255);
		
		// Ponemos que el juego no est� en pausa
		en_pausa = false;
		
		// Creamos nuestro gusaranajo
		gusano = new Sprite(0, 0, img_worm);
		mariquitas_malas = new ArrayList<Sprite>(10);
		colagusano = new Sprite[N_COLAGUSANO];
		for (int i = 0; i < N_COLAGUSANO; i++)
			colagusano[i] = new Sprite(0, 0, img_colagusano);
		
		// Iniciamos las probabilidades
		prob_min = 50;
		probabilidad = prob_min;
		
		// Iniciamos la puntuaci�n del jugador
		score = 0;
		n_mariquitas = 0;
		n_gotas = 0;
		
		init_time = System.currentTimeMillis();
		last_time = init_time;
		elapsed_frames = 0;
		
		update_timer = new Timer();
		update_timer.schedule(new UpdateTask(), 0, UPDATE_DELAY);
	}

	/*
	 * M�todo Update para actualizar la l�gica del juego.
	 */
	private void doUpdate() {
		// Cada 1 segundo miramos si se saca una mariquita-mala
		if (elapsed_frames % 60 == 0) {
			// Comprobamos si podemos sacar una mariquita-mala y si sale una
			if (n_mariquitas < MAX_MARIQUITAS && rand.nextInt(100) <= probabilidad) {
				int org_x = 0, org_y = 0, dest_x = anchoPantalla, dest_y = altoPantalla;
				switch (rand.nextInt(4)) {
					case 0: // Va de Arriba hacia Abajo
						org_x = rand.nextInt(anchoPantalla);
						org_y = 0;
						dest_x = rand.nextInt(anchoPantalla);
						dest_y = altoPantalla;
						break;
					case 1: // Va de Izquierda a Derecha
						org_x = 0;
						org_y = rand.nextInt(altoPantalla);
						dest_x = anchoPantalla;
						dest_y = rand.nextInt(altoPantalla);
						break;
					case 2: // Va de Abajo hacia Arriba
						org_x = rand.nextInt(anchoPantalla);
						org_y = altoPantalla;
						dest_x = rand.nextInt(anchoPantalla);
						dest_y = 0;
						break;
					case 3: // Va de Derecha a Izquierda
						org_x = anchoPantalla;
						org_y = rand.nextInt(altoPantalla);
						dest_x = 0;
						dest_y = rand.nextInt(altoPantalla);
						break;
				}
				Sprite mariquita = new Sprite(org_x, org_y, img_mariquita);
				mariquita.setDestino(dest_x, dest_y);
				mariquita.setDimensionesPantalla(anchoPantalla, altoPantalla);
				mariquitas_malas.add(mariquita);
				n_mariquitas++;
				// Restablecemos las probabilidades para que no salgan segidas
				probabilidad = prob_min;
			} else
				// En caso de que no haya salido, aumentamos la probabilidad
				probabilidad = Math.min(100, probabilidad + 5);
		}
		
		// Cada 5 segundos aumentamos la probabilidad m�nima de sacar una mariquita-mala
		if (elapsed_frames % 300 == 0)
			prob_min = Math.min(100, prob_min + 3);
		
		// Movemos todas las mariquitas-malas en pantalla
		ArrayList<Sprite> lista_borrar = new ArrayList<Sprite>(10);
		Iterator<Sprite> im = mariquitas_malas.iterator();
		while (im.hasNext()) {
			Sprite mariquita = im.next();
			if (mariquita.avanzar(PASO_MOVIMIENTO)) {
				lista_borrar.add(mariquita);
			} else if (mariquita.collide(gusano)) {
				hazMuerte();
				lista_borrar.add(mariquita);
			}
		}
		
		// Borramos las mariquitas-malas que hayan llegado a su destino
		im = lista_borrar.iterator();
		while (im.hasNext()) {
			Sprite mariquita_borrable = im.next();
			mariquitas_malas.remove(mariquita_borrable);
			n_mariquitas--;
		}
		
		// Movemos la cola del gusano
		colagusano[0].moverRespecto(gusano, MOVIMIENTO_COLA);
		for (int i = 1; i < N_COLAGUSANO; i++)
			colagusano[i].moverRespecto(colagusano[i - 1], MOVIMIENTO_COLA);
		
		// Aumentamos los contadores de logica antes de actualizar elementos
		elapsed_frames++;
		score ++;
	}
	
	/*
	 * M�todo Draw que dibuja los cambios hechos en el juego.
	 */
	private void doDraw(Canvas canvas) {
		// Pintamos nuestro gusano
		for (int i = N_COLAGUSANO - 1; i >= 0; i--)
			canvas.drawBitmap(colagusano[i].imagen, colagusano[i].posX, colagusano[i].posY, mObjetoPaint);
        canvas.drawBitmap(gusano.imagen, gusano.posX, gusano.posY, mObjetoPaint);
        
        // Pintamos las mariquitas-malas
        Iterator<Sprite> im = mariquitas_malas.iterator();
        int i = 1;
		while (im.hasNext()) {
			i++;
			Sprite mariquita = im.next();
			canvas.drawBitmap(mariquita.imagen, mariquita.posX, mariquita.posY, mObjetoPaint);
			//String str = mariquita.origX + "," + mariquita.origY + "/" + mariquita.destX + "," + mariquita.destY;
			//canvas.drawText(str, 2, 20 + mTextPaint.getTextSize() * i, mTextPaint);
		}
        
		// Escribimos la puntuaci�n
		//score = (int)((System.currentTimeMillis() - init_time) / 1000);
        canvas.drawText("Puntuacion: " + score, 2f, 20f, mTextPaint);
        canvas.drawText("Prob: " + probabilidad, 2f, 20 + mTextPaint.getTextSize(), mTextPaint);
        //canvas.drawText("rate: " + anchoPantalla + "," + altoPantalla, 2f, 20 + mTextPaint.getTextSize() * 2, mTextPaint);
        canvas.drawText("fps: " + fps, anchoPantalla - mTextPaint.getTextSize() * 5, 20f, mTextPaint);
        invalidate();
	}
	
	private void hazMuerte() {
		score = 0;
	}
}
